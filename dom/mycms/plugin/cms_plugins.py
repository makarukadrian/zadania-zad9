__author__ = 'st'
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
import models

class FacebookFacepilePlugin(CMSPluginBase):
    model = models.FacebookLike
    name = 'Social Facebook Facepile Plugin'
    render_template = 'like.html'

    
    def render(self, context, instance, placeholder):
        context.update({
            'facepile': instance,
            'placeholder': placeholder
        })
        return context

plugin_pool.register_plugin(FacebookFacepilePlugin)