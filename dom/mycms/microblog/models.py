from django.db import models

# Create your models here.


class Post(models.Model):
    tekst = models.CharField(max_length=160)
    tytul = models.CharField(max_length=40)
    uzytkownik = models.CharField(max_length=32)
    data = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return self.tytul