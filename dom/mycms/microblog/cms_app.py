__author__ = 'Samsung'
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

class Blog(CMSApp):
    name = _("Blog")
    urls = ["microblog.urls"]
    render_template = "microblog/index.html"

apphook_pool.register(Blog)