from django.test import TestCase
import unittest
from django.contrib.auth import authenticate
from models import Post

class MicroblogTest(TestCase):

    def testbazy(self):
        Post.objects.create(tytul='x', tekst='x', author='x')
        strona = self.client.get('/blog', follow=True)
        self.assertContains(strona, 'test')

    def testautentykacji(self):
        url = '/blog/zarejestruj'
        self.client.post(url, {'username': 'admin', 'password': 'admin'})
        user = authenticate(username='admin', password='admin')
        self.assertTrue(user)

    def testdod(self):
        Post.objects.create(tekst='SFAF', title=self.tytul, author='admin')
        strona = self.client.get('/blog', follow=True)
        self.assertContains(strona, 'SFAF')

    def testB(self):
        dodawanie = self.client.get('/dodajWpis', follow=True)
        self.assertContains(dodawanie, 'HEHE')

    def testwpisy(self):
        strona = self.client.get('/blog', follow=True)
        self.assertContains(strona, 'test')

    def testwpisyU(self):
        Post.objects.create(tytul='test', tekst='test', author='admin')
        mainPage = self.client.get('/blog', follow=True)
        self.assertContains(mainPage, 'admin')

if __name__ == '__main__':
    unittest.main()