from models import Comment
from forms import CommentForm

from cms.plugin_base import CMSPluginBase, CMSPlugin
from cms.plugin_pool import plugin_pool

class CommentPlugin(CMSPluginBase):
    model = CMSPlugin
    name = "Comment"
    render_template = "comment.html"
    
    def render(self, context, instance, placeholder):
        request = context['request']
        comments = Comment.objects.filter(str=request.current_page.id)
        context.update({
            'instance': instance,
            'placeholder': placeholder,
            'comments': comments,
            'form': CommentForm(),
        })
        return context

plugin_pool.register_plugin(CommentPlugin)