from models import Comment
from forms import CommentForm

from django.shortcuts import redirect
from django.core.urlresolvers import reverse


def com(request, id):
    r = None
    if request.POST:
        r = request.POST.get('return')
        com = Comment(str=id)
        forms = CommentForm(request.POST, instance=com)
        if forms.is_valid():
            forms.save()
    if not r:
        r = ':'
    return redirect(r)
