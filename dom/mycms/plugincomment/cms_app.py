from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

class Comment(CMSApp):
    name = _("Comment")
    urls = ["plugincomment.urls"]
    render_template = "comment.html"

apphook_pool.register(Comment)