from django import forms

class ContactForm(forms.Form):
    email = forms.EmailField(max_length=50)
    tytul = forms.CharField(max_length=50)
    tekst = forms.CharField(max_length=300)