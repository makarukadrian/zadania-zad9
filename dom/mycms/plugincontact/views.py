from django.shortcuts import render_to_response
from django.core.mail import send_mail
from django.shortcuts import redirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from forms import ContactForm

def index(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            send_mail(form.cleaned_data['tytul'], form.cleaned_data['tekst'], form.cleaned_data['email'], ['d4nek@interia.pl'], fail_silently=True)
            form = ContactForm()
            redirect(reverse('email'))
    else:
        form = ContactForm()
    return render_to_response("contact.html", locals(), RequestContext(request))